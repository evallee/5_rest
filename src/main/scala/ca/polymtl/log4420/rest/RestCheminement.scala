package ca.polymtl.log4420
package rest

import model.Cheminement

import net.liftweb._
import common.Box
import http.rest.RestHelper
import json._
import json.JsonAST.JValue
import org.bson.types.ObjectId

import com.foursquare.rogue.Rogue._

object RestCheminement extends RestHelper
{
  serve( "cheminement" :: Nil prefix {

    /*
     * Affiche tous les cheminements
     */
    case Nil JsonGet _ => ???

    /*
    * Affiche un seul cheminement
    */
    case id :: Nil JsonGet _ => ???

    /*
     * Ajoute un cheminement
     */
    case Nil JsonPost ( json -> _ ) => ???

    /*
     * Modifie un cheminement
     */
    case id :: Nil JsonPut ( json -> _ ) => ???


    /*
     * Suprime un cheminement
     */
    case id :: Nil JsonDelete _ => ???
  })
}