package ca.polymtl.log4420
package rest

import model.Cheminement

import net.liftweb._
import common._
import common.Box._
import http.rest.RestHelper
import json.JsonAST.JValue

import util.BasicTypesHelpers._

object RestSession extends RestHelper
{
  serve( "cheminement" :: Nil prefix {

        /*
         * Affiche la ième session
         */
        case id :: "session" :: i :: Nil  JsonGet _ =>
        {
            for {
              sessionIndex <- asInt( i ) ?~ "index session pas un int" ~> 400
              cheminement <- Cheminement.findById( id ) ?~ "cheminement introuvable" ~> 404
              sessions <- cheminement.sessions.valueBox ?~ "pas de session" ~> 500
              ( session, _ ) <- sessions.zipWithIndex.find{ case ( _, j ) => j == sessionIndex } ?~ "session introuvable" ~> 404
            } yield cheminement
        }.open_!.sessions.value.head : JValue

        /*
         * Affiche le jième cours de la ième session
         */
        case id :: "session" :: i :: "cours" :: j :: Nil  JsonGet _ => ???

        /*
         * Ajoute une session à la fin
         */
        case id :: "session" :: Nil JsonPost _ => ???
  })
}
